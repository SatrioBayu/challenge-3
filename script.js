const btn = document.querySelector(".navbar-toggler");
btn.addEventListener("click", function () {
  const body = document.querySelector(".blocking");
  body.style.display = "block";

  body.addEventListener("click", function () {
    this.style.display = "none";
  });
  const closeBtn = document.querySelector(".close");
  closeBtn.addEventListener("click", function () {
    body.style.display = "none";
  });
});
